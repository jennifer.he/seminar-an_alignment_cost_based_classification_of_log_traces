import pm4py
from pm4py.objects.petri_net.importer import importer as pnml_importer
import time
from pm4py.algo.conformance.alignments.petri_net import algorithm as alignments
from pm4py.algo.filtering.log.variants import variants_filter

import sys

'''
Author : Boltenhagen Mathilde
Date : June 2020
Adapted by He, Jennifer December 2021

createAlignments.py : compute alignment costs with pm4py 
'''

# read parameter : python3 createAlignments.py output_file xes_file pnml_file
argv=sys.argv
filename=argv[1]
net, m0, mf = pnml_importer.apply(argv[3])
log = pm4py.read_xes(argv[2])

# compute alignment for variants only
variants = variants_filter.get_variants(log)


start=time.time()
for l in variants:
    ali=alignments.apply(variants[l][0],net, m0,mf,variant=alignments.Variants.VERSION_DIJKSTRA_NO_HEURISTICS)
    # export in a CSV file with : trace, trace with moves, run, run with moves, cost, frequency
    if ali:
        writer=open(filename,"a")
        alignment=ali["alignment"]
        cost=ali["cost"]
        phrase1=""
        phrase1None=""
        phrase2=""
        phrase2None=""
        for (a,b) in alignment:
            if a!=">>":
                # activities are separated with :::
                phrase1+=a+":::"
            phrase1None+=a+":::"
            if b!=">>" and b!=None:
                phrase2+=b+":::"
            phrase2None+=str(b)+":::"
        writer.write(phrase1+";"+phrase1None+";"+phrase2+";"+phrase2None+";"+str(cost)+";"+str(len(variants[l]))+"\n")
        writer.close()

writer=open(filename,"a")
writer.write(str(start-time.time()))
writer.close()